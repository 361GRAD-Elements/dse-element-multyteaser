<?php

/**
 * 361GRAD Element Multyteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_multyteaser'] =
    'Dse\\ElementsBundle\\ElementMultyteaser\\Element\\ContentDseMultyteaser';
